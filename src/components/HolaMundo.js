import React from "react";

function HolaMundo(props) {
  console.log(props);
  const { nombre, edad, color } = props.userInfo;
  return (
    <div>
      <>
        Hola Mundo en ReactJS, por {nombre}, que tiene {edad} años y su color
        favorito es {color}.
      </>
    </div>
  );
}


export function Adios() {
  return (
    <div>
      <>Aprendiendo a programar 😊</>
    </div>
  );
}

export default HolaMundo;
