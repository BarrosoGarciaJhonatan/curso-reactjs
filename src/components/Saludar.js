import React from "react";

export default function Saludar(props) {
  console.log("Props del componente saludar: ");
  console.log(props);
  const { userInfo, fnSaludar } = props;
  const { nombre = "Anonimo", edad, color } = userInfo;
  /*const generaSaludo = (name) => {
    alert("Hola " + name);
  };*/
  return (
    <div>
      <button onClick={() => fnSaludar(nombre, edad, color)}>Saludar</button>
    </div>
  );
}
