// import logo from "./logo.svg";
import "./App.css";
// import Hola, { Adios } from "./components/HolaMundo";
// import Saludar from "./components/Saludar";
// import Contador from "./components/Contador";
import Galeria from "./components/Galeria";
function App() {
  // const userName = "Jhonatan BG";
  // const edad = 24;
  /**const user = {
    nombre: "Jhonatan BG",
    edad: 24,
    color: "Azul",
  };
  const user2 = {
    edad: 24,
    color: "Azul",
  };
  const generaSaludo = (nombre, edad, color) => {
    alert(
      "Hola " +
        nombre +
        ", tienes " +
        edad +
        " años, y tu color favorito es " +
        color
    );
    alert(
      `Hola ${nombre}, tienes ${edad} años, y tu color favorito es ${color}.`
    );
  };**/

  return (
    <div className="App">
      <header className="App-header">
        {/*<img src={logo} className="App-logo" alt="logo" />
        <div>
          <Hola userInfo={user}></Hola>
          <Adios></Adios>
          <Saludar userInfo={user2} fnSaludar={generaSaludo} />
          <Contador />
  </div>*/}
        <Galeria />
      </header>
    </div>
  );
}

export default App;
